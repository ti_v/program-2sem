from math import tan, acos

''' Вариант 8 '''

a = float(input('Введите а: '))
xMax = int(input('Введите максимальное значение x: '))
xMin = int(input('Введите минимальное значение x: '))
stepCount = int(input('Введите количество шагов для вычисления функции: '))

# Массивы для хранения входной переменной и полученного значения
x1 = []
y1 = []

if xMin >= xMax:
    print('Максимальное значение {0} больше или равно минимальному {1}'.format(xMax, xMin))
    exit()

n = int(input('Выберите функцию для вычисления: \n   1 - функция g \n   2 - функция f \n   3 - функция y\nНомер: '))


def calc(a, x):
    if 1 <= n <= 3:
        if n == 1:
            g = 5 * ((-9 * a ** 2) - (11 * a * x) + (14 * x ** 2)) / ((15 * a ** 2) + (49 * a * x) + (24 * x ** 2))
            x1.append(x)
            y1.append(g)
        elif n == 2:
            try:
                x1.append(x)
                y1.append(tan((18 * a ** 2) + (29 * a * x) + (10 * x ** 2)))
            except ValueError:
                pass
        elif n == 3:
            try:
                x1.append(x)
                y1.append(acos((-7 * a ** 2) - (10 * a * x) + (8 * x ** 2) + 1))
            except ValueError:
                pass
    else:
        print('Такой функции не найдено')
        pass


count = 0
while count < stepCount:
    x = xMin + count / 10
    if x <= xMax:
        calc(a, x)
        count += 1
    else:
        break

# Формирование таблицы и нахождения max и min значений
for result in zip(x1, y1):
    print('{0:.3f}  {1:.3f}'.format(*result), sep='    ')

print('Максимальное значение функции: ' + str(max(y1)))
print('Минимальное значение функции: ' + str(min(y1)))